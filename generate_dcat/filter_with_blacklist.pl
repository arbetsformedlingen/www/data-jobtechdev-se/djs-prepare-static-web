#!/usr/bin/perl
# -*- mode: cperl;-*-
use warnings;
use strict;
use FindBin;
use lib $FindBin::Bin."/../lib";
use Blacklist;

map { print $_ }
  grep { ! Blacklist::is_blacklisted($_) }
  <STDIN>;

#!/usr/bin/perl
use warnings;
use strict;
use File::Temp qw/ :POSIX /;
use File::Slurp;
use JSON;
use URI::Escape;

my $colldcat = shift(@ARGV);
my $datafilelist = shift(@ARGV);

my $datafiles = read_file($datafilelist);
my $jsondata = read_file($colldcat);

my $json = from_json($jsondata);

$datafiles =~ s/\n/  /g;

$json->{"description-sv"} .= "    $datafiles";
$json->{"description-en"} .= "    $datafiles";

my $jsonobj = JSON->new->allow_nonref;
print $jsonobj->pretty->encode($json);

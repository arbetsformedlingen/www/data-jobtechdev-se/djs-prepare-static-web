#!/usr/bin/perl
# -*- mode: cperl;-*-
use warnings;
use strict;
use File::Slurp;
use FindBin;
use lib $FindBin::Bin."/../lib";
use Blacklist;

my $bucket = defined($ENV{BUCKET}) ? $ENV{BUCKET} : "data.jobtechdev.se-adhoc-test";

my %files;

map { $files{$_} = 1 }
  grep { ! Blacklist::is_blacklisted($_) }
  map { $_ =~ s/^.{30} //; $_ }
  `aws s3 ls --recursive s3://$bucket/`;

map { print $_ }
  grep { my $dcat = $_; $dcat =~ s/.dcat.json$//; defined($files{$dcat}) }
  grep { /.dcat.json$/ }
  keys %files;

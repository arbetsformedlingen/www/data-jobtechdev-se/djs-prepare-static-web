# Publishing

## Links

### development/test
- The S3 bucket: s3://data.jobtechdev.se-adhoc-test/
- Data Jobtechdev se: https://data-develop.jobtechdev.se/
- Dataportalen: https://sandbox.admin.dataportal.se/organization/16/status
https://www-sandbox.dataportal.se/

### prod
- The S3 bucket: s3://data.jobtechdev.se-adhoc/
- Data Jobtechdev se: https://data.jobtechdev.se/
- Dataportalen: https://www.dataportal.se/


## Publishing metadata of APIs

### Prerequisites
An API record needs to contain DCAT information. The record itself
can be served either from a REST server (typically as `swagger.json`),
or reside in this repository:
<https://gitlab.com/arbetsformedlingen/metadata> (commited to `main`).

For the test environment, use
<https://gitlab.com/arbetsformedlingen/metadata-test>.


Use only OAS3 JSON format. If you have a raml file, convert it to OAS3
JSON first.

The file which Dataportalen harvests resides is generated here:
https://data.jobtechdev.se/dcatotron/jobtech.dcat.xml


### Changing existing records
If the API specification resides in
<https://gitlab.com/arbetsformedlingen/metadata>, then make the
desired change there and commit to `main`.

If the API specification is served from a REST server, the information
needs to be updated there.

The changes will be automatically propagated to
`https://data.jobtechdev.se` and `https://dataportalen.se`. It can
take up to an hour for the change to show (perhaps even longer if
Dataportalen harvests data with longer intervals).

### Adding a record
Add a link to the new API specification to this file:
<https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web/-/blob/main/generate_dcat/conf/api-json-spec-urls.txt>
and commit the change to `master`.


### Removing a record
Remove the link to the API specification from this file:
<https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web/-/blob/main/generate_dcat/conf/api-json-spec-urls.txt>
and commit the change to `master`.

### Editing the catalog info
Make the necessary changes to
<https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web/-/blob/main/generate_dcat/conf/catalog.json>
and commit to `master`.


## Publishing data sets
Below are listed a few different methods of publishing data files.

Avoid mixing these methods in the same directory - select only one.


### Publishing individual data files
Use this method if you have data files which need individual metadata,
as opposed to files which are part of a data set.

Upload the file `F` to publish to the S3 bucket. Then create a file
called `F.dcat.json` and place in the same directory. Use this file as
template:
<https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web/-/blob/main/generate_dcat/conf/example_dcat_for_datafile.json>.


### Publishing a directory of data files as individual objects
Use this method of you have a set of files in a directory, but you
want to apply the same metadata to all files, creating an individual
item for each file in Dataportalen.

Upload the datafiles to a directory in the S3 bucket. Then upload a
file called `directory.dcat.json` in to the same directory. Use this
file as template, and edit the contents:
<https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web/-/blob/main/generate_dcat/conf/directory.dcat.json>.


### Publishing a directory of data files as a single data set
Use this method if you have a set of files in a directory which you
want to publish as a single entry in Dataportalen. The download link
will point back to its directory page in data.jobtechdev.se.

Upload the datafiles to a directory in the S3 bucket. Then upload a
file called `directory_link.dcat.json` in to the same directory. Use this
file as template, and edit the contents:
<https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web/-/blob/main/generate_dcat/conf/directory_link.dcat.json>.

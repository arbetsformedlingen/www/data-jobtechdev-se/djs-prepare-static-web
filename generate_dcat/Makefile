
SHELL = /bin/bash
.SHELLFLAGS := -eu -o pipefail -c # Bash strict mode is a blessing
.ONESHELL:

BUCKET ?= data.jobtechdev.se-adhoc-test

S3HOST ?= s3-website.eu-central-1.amazonaws.com

BUCKETDIR = dcatotron

BASEURL ?= https://data-develop.jobtechdev.se/

DCATJAR ?= /opt/app.jar

export PATH := $(PWD):$(PATH)

# default to test
DJS_ENV ?= test

ifeq ($(DJS_ENV), test)
API_URL_LIST = https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web/-/raw/main/generate_dcat/conf/api-json-spec-urls-test.txt
else
API_URL_LIST = https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web/-/raw/main/generate_dcat/conf/api-json-spec-urls.txt
endif
$(info Using api-url-list $($API_URL_LIST))


all: verify-env mkdir download-specs download-s3-specs process-directory-link-dcat process-directory-dcat validate-specs target/jobtech.dcat.xml upload-dcat-to-djs upload-filedcats-to-djs



local:
	$(MAKE)	DCATJAR=$(PWD)/dev/app.jar PATH=$(PATH):$(PWD)


upload-dcat-to-djs:
	@echo "INFO] Uploading target/jobtech.dcat.xml to s3://$(BUCKET)/$(BUCKETDIR)..." >&2
	if ! aws s3 cp --content-type=text/xml --no-guess-mime-type --metadata-directive=REPLACE target/jobtech.dcat.xml s3://$(BUCKET)/$(BUCKETDIR)/; then
		echo "ERROR] Could not upload to S3" >&2; exit 1
	fi


upload-filedcats-to-djs:
	@cd meta
	for FILE in $$(ls -1 *.s3); do
		BASENAME="$$(echo $$FILE | sed 's|.dcat.json.s3$$||')"
		S3ADDR="$$(cat $$FILE)"
		if [ -f "$$BASENAME".dcat.xml ]; then
			S3BASENAME="$$(echo $$S3ADDR | sed 's|.dcat.json$$||')"
			aws s3 cp "$$BASENAME".dcat.xml s3://$(BUCKET)/"$$S3BASENAME".dcat.xml
		fi
	done


# The rule starts by copying a catalog.json file from the conf
# directory to the target directory. It then prints an informational
# message to the standard error output, indicating that it's about to
# convert the specification files in the target directory to DCAT
# format.
#
# Next, it changes the current directory to target and runs a Java
# program using the java -jar command. The Java program is presumably
# a DCAT converter, and it's passed the current directory ($$PWD) as
# an argument. The output of the Java program is redirected to the
# jobtech.dcat.xml file in the target directory.
#
# The rule then checks the generated DCAT file for errors. If the file
# contains certain error messages, the rule prints an error message,
# outputs the contents of the DCAT file to the standard error output,
# and exits with a status of 1.
#
# Next, the rule checks the DCAT file for lines containing "sun
# characters" (represented by the ¤ symbol). If such lines are found,
# it prints an informational message, outputs the lines with sun
# characters to the standard error output, and removes these lines
# from the DCAT file. The sponge command is used to write the output
# back to the DCAT file.
#
# Finally, the rule removes a file named dcat.rdf, presumably a
# temporary file used during the conversion process. The # FIXME
# comment indicates that the removal of sun characters is a temporary
# patch that should be removed in the future.
#
# catalog.json contains info general to all APIs (see docs at https://github.com/DIGGSweden/DCAT-AP-SE-Processor)
target/jobtech.dcat.xml:
	@echo "INFO] Copy conf/catalog.json to target/" >&2
	cp $(PWD)/conf/catalog.json $(PWD)/target/
	echo "INFO] Converting specification files $$(echo $$(ls -1 $$PWD/target)) to DCAT..." >&2
	cd target # writeable folder in OpenShift
	# FIXME: remove below patch which removes SUN-characters
	LANG=sv_SE.UTF.8 java -jar $(DCATJAR) -d "$$PWD" >"$(PWD)/$@"
	if grep -E -q "(Kunde inte generera en dcat fil|There are Errors)" "$(PWD)/$@"; then
		echo "ERROR] Cannot convert specification files to DCAT" >&2
		cat "$(PWD)/$@" >&2
		exit 1
	fi
	if grep -q -E '>(sv|en)¤<' <"$(PWD)/$@"; then
		echo "INFO] REMOVING LINES WITH SUN CHARACTERS:" >&2
		grep -E '>(sv|en)¤<' <"$(PWD)/$@" >&2
		grep -v -E '>(sv|en)¤<' <"$(PWD)/$@" | sponge "$(PWD)/$@"
	fi
	rm -f dcat.rdf



# The process-directory-dcat rule in this Makefile is designed to
# process a directory of files stored in an AWS S3 bucket.
#
# The rule starts by changing the current directory to staging using
# the cd command. It then prints an informational message to the
# standard error output to indicate that the process-directory-dcat
# process has started.
#
# Next, it checks if there are any files named directory.dcat.json in
# the S3 bucket. This is done using the aws s3 ls --recursive command,
# which lists all files in the bucket recursively. The output of this
# command is piped to grep -q directory.dcat.json, which checks if the
# string directory.dcat.json is present in the output.
#
# If directory.dcat.json files are found, the rule enters a loop where
# it processes each directory.dcat.json file. Inside the loop, it
# first downloads the directory.dcat.json file using the wget
# command. If the download fails, it prints a warning message and
# skips to the next iteration of the loop.
#
# Once the file is successfully downloaded, it lists all files in the
# same directory in the S3 bucket, filters out directories and files
# ending with .dcat.xml, and processes each remaining file. The
# processing involves generating a unique filename using uuidgen,
# preparing a new file with the .dcat.json extension, and writing some
# information to this new file.
#
# Finally, after all files in the directory have been processed, it
# removes the downloaded directory.dcat.json file.
process-directory-dcat:
	cd staging
	echo "INFO] process-directory-dcat..." >&2
	if aws s3 ls --recursive s3://$(BUCKET)/ | tee -p /dev/null | grep -q directory.dcat.json; then
		aws s3 ls --recursive s3://$(BUCKET)/ | tee -p /dev/null | grep directory.dcat.json | perl -pe 's/^.{30} //' | while read DIRDCAT; do
			echo "INFO] Downloading $(BASEURL)$$DIRDCAT..." >&2
			if ! wget --no-check-certificate --content-disposition --no-use-server-timestamps --quiet "$(BASEURL)$$DIRDCAT"; then
				echo "WARNING] Cannot download $(BASEURL)$$DIRDCAT" >&2
				continue
			fi
			DIR="$$(dirname $$DIRDCAT)"
			aws s3 ls s3://$(BUCKET)/"$$DIR"/ | tee -p /dev/null | grep -v "/" | perl -pe 's/^.{30} //;' | filter_with_blacklist.pl | grep -v '.dcat.xml$$' | while read DATAFILE; do
				echo "INFO] Preparing $(BASEURL)$$DIR/$$DATAFILE.dcat.json..." >&2
				FILEUNIQ="$$(uuidgen)"_"$$DATAFILE".dcat.json
				env title_sv="$$DIR/$$DATAFILE" title_en="$$DIR/$$DATAFILE" dcatjson2dcatswagger.sh --input "directory.dcat.json" --inputid "$(BASEURL)$$DIR/$$DATAFILE" >"$$FILEUNIQ"
				echo "$$DIR/$$DATAFILE" > "$$FILEUNIQ".s3
				#C=$$(( C + 1 ))
				#if [ $$C -gt 10 ]; then exit; fi
			done
			rm -f directory.dcat.json
		done
	fi


# The process-directory-link-dcat rule in this Makefile is designed to
# process a directory of files stored in an AWS S3 bucket,
# specifically looking for files named directory_link.dcat.json.
#
# The rule starts by changing the current directory to staging using
# the cd command. It then prints an informational message to the
# standard error output to indicate that the
# process-directory-link-dcat process has started.
#
# Next, it checks if there are any files named
# directory_link.dcat.json in the S3 bucket. This is done using the
# aws s3 ls --recursive command, which lists all files in the bucket
# recursively. The output of this command is piped to grep -q
# directory_link.dcat.json, which checks if the string
# directory_link.dcat.json is present in the output.
#
# If directory_link.dcat.json files are found, the rule enters a loop
# where it processes each directory_link.dcat.json file. Inside the
# loop, it first downloads the directory_link.dcat.json file using the
# wget command. If the download fails, it prints a warning message and
# skips to the next iteration of the loop.
#
# Once the file is successfully downloaded, it generates a unique
# filename using uuidgen and copies the downloaded file to a new file
# with the .w_desc.dcat.json extension. This copy operation is marked
# with a Fixme comment, indicating that it might be a temporary
# solution or something that needs to be revised in the future.
#
# The rule then runs the dcatjson2dcatswagger.sh script on the copied
# file, passing in several parameters including the input file, the
# input ID, the download URL, and the access URL. The output of this
# script is redirected to a new file with the .dcat.json extension.
#
# Finally, it writes the name of the original directory_link.dcat.json
# file to a new file with the .dcat.json.s3 extension, and removes the
# downloaded directory_link.dcat.json file and the copied
# .w_desc.dcat.json file.
process-directory-link-dcat:
	cd staging
	echo "INFO] process-directory-link-dcat..." >&2
	if aws s3 ls --recursive s3://$(BUCKET)/ | tee -p /dev/null | grep -q directory_link.dcat.json; then
		aws s3 ls --recursive s3://$(BUCKET)/ | tee -p /dev/null | grep directory_link.dcat.json | perl -pe 's/^.{30} //' | while read DIRDCAT; do
			echo "INFO] Downloading $(BASEURL)$$DIRDCAT..." >&2
			if ! wget --no-check-certificate --content-disposition --no-use-server-timestamps --quiet "$(BASEURL)$$DIRDCAT"; then
				echo "WARNING] Cannot download $(BASEURL)$$DIRDCAT" >&2
				continue
			fi
			DIR="$$(dirname $$DIRDCAT)"
			COLLFILE="$$(uuidgen)"
			# Fixme: remove this cp
			cp $$(basename "$$DIRDCAT") "$$COLLFILE".w_desc.dcat.json
			dcatjson2dcatswagger.sh --input "$$COLLFILE".w_desc.dcat.json --inputid "$(BASEURL)$$DIR/" --downloadurl "" --accessurl "$(BASEURL)$$DIR/" >"$$COLLFILE".dcat.json
			echo "$$DIRDCAT" > "$$COLLFILE".dcat.json.s3
			rm -f directory_link.dcat.json "$$COLLFILE".w_desc.dcat.json
		done
	fi



# The download-s3-specs rule in this Makefile is designed to download
# and process files from an AWS S3 bucket.
#
# The rule starts by changing the current directory to staging using
# the cd command. It then runs the dcat_finder.pl Perl script, which
# presumably lists files in some way. The output of this script is
# piped to two grep -v commands, which filter out any lines containing
# directory.dcat.json or directory_link.dcat.json.
#
# For each remaining line of output (each representing a file), the
# rule enters a loop. Inside the loop, it first attempts to download
# the file using the wget command. If the download fails, it prints a
# warning message and skips to the next iteration of the loop.
#
# Once the file is successfully downloaded, it renames the file to a
# semi-unique name to lessen the risk of overwriting files with the
# same name. This is done by appending a UUID (generated by uuidgen)
# to the original filename.
#
# The rule then processes the downloaded file using the
# dcatjson2dcatswagger.sh script. The output of this script is piped
# to sed 's|\\n| |g', which replaces all newline characters with
# spaces. The result is redirected to a new file with the semi-unique
# filename.
#
# Finally, it writes the name of the original S3 file to a new file
# with the .s3 extension, and removes the downloaded file.
download-s3-specs:
	@cd staging
	dcat_finder.pl | grep -v directory.dcat.json | grep -v directory_link.dcat.json | while read S3FILE; do
		echo "INFO] Downloading $(BASEURL)$$S3FILE..." >&2
		if ! wget --no-check-certificate --content-disposition --no-use-server-timestamps --quiet "$(BASEURL)$$S3FILE"; then
			echo "WARNING] Cannot download $(BASEURL)$$S3FILE" >&2
			continue
		fi
		# rename files semiuniquely to lessen risk of overwriting files with the same name
		FILE="$$(ls -Art | tail -n 1)"
		FILEUNIQ="$$(uuidgen)"_"$$FILE"
		DATAFILE="$$(echo $$S3FILE | sed 's|.dcat.json$$||')"
		# FIXME: remove ugly fix when Jonas is ready
		dcatjson2dcatswagger.sh --input "$$FILE" --inputid "$(BASEURL)$$DATAFILE" | sed 's|\\n| |g' >"$$FILEUNIQ"
		echo "$$S3FILE" > "$$FILEUNIQ".s3
		rm -f "$$FILE"
	done


download-specs:
	@cd staging
	curl -s $(API_URL_LIST) | while read URL; do
		echo "INFO] Downloading $$URL..." >&2
		FILEUNIQ="$$(uuidgen)".dcat.json
		if ! wget --no-check-certificate --content-disposition --no-use-server-timestamps -O "$$FILEUNIQ" --quiet "$$URL"; then
			echo "WARNING] Cannot download $$URL" >&2
			continue
		fi
	done


validate-specs:
	@cd staging # writeable in OpenShift
	ls -1 *.dcat.json | while read FILE; do
		echo "INFO] Validating $$FILE..." >&2
		if ! jq -r .openapi < "$$FILE" | grep -q "^3"; then
			echo "WARNING] Cannot validate $$FILE: not openapi 3" >&2
			continue
		fi
		BASENAME="$$(echo $$FILE | sed 's|.dcat.json$$||')"
		LANG=sv_SE.UTF.8 java -jar $(DCATJAR) -f "$$FILE" 2>&1 > "$$BASENAME".dcat.xml
		if grep -q "There are Errors in the following files:" "$$BASENAME".dcat.xml; then
			echo "WARNING] Cannot validate $$FILE: dcat processor failure" >&2
			cat "$$BASENAME".dcat.xml >&2
			rm "$$BASENAME".dcat.xml
			continue
		fi
		rm -f dcat.rdf output.json
		mv "$$FILE" $(PWD)/target/ || exit 1
		mv "$$BASENAME".dcat.xml $(PWD)/meta/ || exit 1
		if [ -f "$$FILE".s3 ]; then
			mv "$$FILE".s3 $(PWD)/meta/
		fi
	done


mkdir:
	@mkdir -p target staging meta


verify-env:
	@for T in jq java aws wget uuidgen; do
		if ! command -v $$T &> /dev/null; then
			echo "ERROR] please install $$T" >&2; exit 1
		fi
	done


clean:
	rm -rf target staging meta

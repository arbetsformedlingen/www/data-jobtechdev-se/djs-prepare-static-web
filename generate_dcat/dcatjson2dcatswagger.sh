#!/usr/bin/env bash
set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

swagger_template="$self_dir"/conf/swaggerwrapper.json
catalog="$self_dir"/conf/catalog.json

title_sv="${title_sv:-}"
title_en="${title_en:-}"

tmpfile=$(mktemp)
tmpfile2=$(mktemp)

trap "rm -f $tmpfile $tmpfile2" EXIT


downloadurl=
accessurl=
while [ "$#" -gt 0 ]; do
    case "$1" in
        --input)       input="$2"; shift;;
        --inputid)     inputid="$2"; shift;;
        --accessurl)   accessurl="$2"; shift;;
        --downloadurl) downloadurl="$2"; shift;;
        *)               if [[ $1 =~ ^- ]]; then echo "unknown opt $1, try $0 -h" >&2; exit 1; else break; fi;;
    esac
    shift
done

if [ ! -f "$input" ]; then echo "**** no such input file: $input">&2; exit 1; fi
if [ -z "$accessurl" ]; then   accessurl="$inputid"; fi



jq -s '.[0] * { info: { "x-dcat": { "dcat-dataset": .[1] } } }' "$swagger_template" "$input" > "$tmpfile"

jq -s '.[0] * { info: { "x-dcat": .[1] } }' "$tmpfile" "$catalog" \
         | jq --arg url     "$inputid"         '.info."x-dcat"."dcat-dataset".distribution.accessURL = $url'   \
         |  { if [ "$downloadurl" = "" ]; then cat; else jq --arg url     "$inputid"         '.info."x-dcat"."dcat-dataset".distribution.downloadURL = $url' ; fi }\
         | jq --arg contact "$inputid#contact" '.info."x-dcat"."dcat-dataset".contactPoint.about = $contact'   \
         | jq --arg about   "$inputid#about0"  '.info."x-dcat"."dcat-dataset".about = $about' \
         | { if [ "$title_sv" = "" ]; then cat; else jq --arg title "$title_sv" '.info."x-dcat"."dcat-dataset"."title-sv" = (.info."x-dcat"."dcat-dataset"."title-sv" + " - " + $title) | .info."x-dcat"."dcat-dataset".distribution."title-sv" = (.info."x-dcat"."dcat-dataset".distribution."title-sv" + " - " + $title)'; fi } \
         | { if [ "$title_en" = "" ]; then cat; else jq --arg title "$title_en" '.info."x-dcat"."dcat-dataset"."title-en" = (.info."x-dcat"."dcat-dataset"."title-en" + " - " + $title) | .info."x-dcat"."dcat-dataset".distribution."title-en" = (.info."x-dcat"."dcat-dataset".distribution."title-en" + " - " + $title)'; fi } > "$tmpfile2"

## Some input documents use .distribution1, ..., .distributionN instead of just .distribution.
## In those cases we need to delete the default .distribution
if [ "$(jq '.distribution' < $input)" != null ]; then
    < "$tmpfile2" jq --arg distrib "$inputid#distrib" '.info."x-dcat"."dcat-dataset".distribution.about = $distrib'
else
    < "$tmpfile2" jq 'del(.info."x-dcat"."dcat-dataset".distribution)'
fi

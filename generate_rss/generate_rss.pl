#!/usr/bin/perl
#
# Requires a configured aws command in the environment.
#
#
use warnings;
use strict;
use File::Slurp;
use File::Temp qw/ :POSIX /;
use DateTime;
use POSIX ();
use URL::Encode::PP qw/ url_encode /;
use FindBin;
use lib $FindBin::Bin."/../lib";
use Blacklist;
use String::ShellQuote;


my $bucket = defined($ENV{BUCKET}) ? $ENV{BUCKET} : "data.jobtechdev.se-adhoc-test";
my $baseurl = defined($ENV{BASEURL}) ? $ENV{BASEURL} : "https://djs-frontend-data-jobtechdev-se-develop.test.services.jtech.se/";
my $manual_checksum_list = 'conf/manual_checksum_list.txt';


POSIX::setlocale(POSIX::LC_TIME, "C.utf8");
my $pubdate = DateTime->now->strftime("%a, %d %b %Y %H:%M:%S %z", localtime(time()));



sub diff {
    my ($array0,$array1) = @_;
    my %count = ();
    my @removed = ();
    my @modified = ();
    my @added = ();

    # pick out the filename as key (starting at index 31 in the strings
    for my $element (@{$array0}) { my ($n) = $element =~ /^\S+ (.+)$/; if(defined($n)) { $count{$n}->{array0} = $element } }
    for my $element (@{$array1}) { my ($n) = $element =~ /^\S+ (.+)$/; if(defined($n)) { $count{$n}->{array1} = $element } }

    for my $element (keys %count) {
        next if(Blacklist::is_blacklisted($element));
        if(defined($count{$element}->{array0})
           && ! defined($count{$element}->{array1})) {
            push(@removed, $element);
        } elsif(! defined($count{$element}->{array0})
                && defined($count{$element}->{array1})) {
            push(@added, $element);
        } elsif($count{$element}->{array0} ne $count{$element}->{array1}) {
            push(@modified, $element);
        }
    }

    return (\@removed, \@modified, \@added);
}


sub read_s3_file_list {
    my $path = shift;

    grep { !/\/$/ }
    grep { !/index.html$/ }
    grep { !/50x.html$/ }
    split("\n", `aws s3 ls --recursive $path`);
}




sub format_rss_msg {
    my ($items) = @_;

    my $xml = q`<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Open Data at Jobtech Dev</title>
    <link>$baseurl/swagger</link>
    <description>Open Data at Jobtech Dev</description>
    <atom:link href="https://$bucket/rss/datajobtechdevse.xml" rel="self" type="application/rss+xml" />`;

    $xml .= $items;

    $xml .= q`
  </channel>
</rss>
`;

    return $xml;
}



sub create_rss_items {
    my ($removed, $modified, $added) = @_;
    my $xml = "";

    foreach my $section ( { type => "removed", data => $removed },
                          { type => "modified", data => $modified },
                          { type => "added", data => $added }) {
        foreach my $data (@{$section->{data}}) {
            print STDERR "$pubdate]\t$section->{type} $data\n";
            $xml .= "<item>
    <title>$data</title>
    <link>$baseurl$data</link>
    <guid>$baseurl$data</guid>
    <description>data item has been $section->{type}</description>
    <pubDate>$pubdate</pubDate>
</item>\n";
        }
    }

    return $xml;
}



sub get_checksums {
    my $exceptionsref = shift;
    my $filelistref = shift;

    return map {
        my $line = $_;
        my ($filename) = $line =~ /^.{30} (.+)$/;
        $filename = shell_quote($filename);
        $filename =~ s/^'//;
        $filename =~ s/'$//;
        my $patchscript = "";
        foreach my $ptrn (keys %{$exceptionsref}) {
            if($filename =~ /$ptrn/) {
                $patchscript = $exceptionsref->{$ptrn};
                die("cannot find patch script $patchscript for $filename") unless(-f $patchscript);
                last;
            }
        }

        if($patchscript) {
            my $tmpfile = tmpnam();
            die("cannot download $bucket/$filename") unless(system("aws s3 cp 's3://$bucket/$filename' $tmpfile") == 0);
            my $md5 = `$patchscript < $tmpfile | md5sum`;
            unlink($tmpfile);
            $md5 =~ s/^(\S+)\s.*$/$1/;
            print STDERR "MD5 for $filename: $md5\n";
            $_ = sprintf("%.30s %s", $md5, $filename);
        } else {
            my $curlurl = url_encode($filename);
            $curlurl =~ s/\+/%20/g;
            print STDERR "curl -k --silent --head '$baseurl$curlurl'\n";
            my ($etag) = map { $_ =~ s/\r//gs; $_; } grep { /^ETag: /i } split("\n", `curl -k --silent --head '$baseurl$curlurl'`);
            if($etag =~ s/^etag: "(.+)"/$1/i) {
                $_ = sprintf("%.30s %s", $etag, $filename);
            } else {
                die("Unexpected input: $etag");
            }
        }
    } @{$filelistref};
}


### MAIN

die("cannot get rss folder")
    unless(system("aws s3 cp --recursive s3://$bucket/rss/ rss") == 0);


my %exceptions = ();
map {
    if($_ =~ /^\s*(\S+)\s+(\S+)\s*$/) {
        $exceptions{$2} = $1;
    }
} split("\n", read_file($manual_checksum_list));


my @l0sum = split("\n", read_file('rss/filelist'));

my @l1 = read_s3_file_list "s3://$bucket/";

my @l1sum = get_checksums(\%exceptions, \@l1);

my $oldxmlitems = read_file('rss/items');

my $xmlitems = create_rss_items(diff(\@l0sum, \@l1sum));

my $xml = format_rss_msg($oldxmlitems.$xmlitems);

write_file('rss/filelist',  join("\n", @l1sum)."\n");
write_file('rss/items', $oldxmlitems.$xmlitems);
write_file('rss/datajobtechdevse.xml', $xml);

die("cannot write rss folder")
    unless(system("aws s3 cp --content-type=application/rss+xml --no-guess-mime-type --metadata-directive=REPLACE --recursive rss/ s3://$bucket/rss/") == 0);

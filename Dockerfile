FROM registry.gitlab.com/arbetsformedlingen/devops/dcat-ap-se-processor-mirror/18-f-rvaltning-av-gamla-beroenden-1:f9ca2de3528294cf9aed6505de05cd523f18bf3d AS jar-image


FROM ubuntu:22.04 AS app

COPY --from=jar-image /opt/app.jar /opt/

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -y update \
    && apt-get -y install make awscli moreutils wget uuid-runtime rclone \
    awscli libfile-slurp-perl liburi-perl liburl-encode-perl openjdk-21-jre \
    locales libjson-perl libdatetime-perl libstring-shellquote-perl curl jq \
    && locale-gen sv_SE.UTF-8 \
    && update-locale



###############################################################################
FROM app AS test

RUN wget https://github.com/diggsweden/DCAT-AP-SE-Processor/raw/18-f%C3%B6rvaltning-av-gamla-beroenden-1/src/main/resources/metadataExample/single/full_example.json \
    && java -jar /opt/app.jar -f full_example.json > dcat.rdf \
    && cat dcat.rdf \
    && ! grep -q "Kunde inte generera en dcat fil" dcat.rdf || { echo "ERROR: test failed" >&2; exit 1; } \
    && rm -f full_example_oas.json dcat.rdf \
    && touch /.tests-successful



###############################################################################
FROM app

COPY --from=test /.tests-successful /

WORKDIR /app

COPY generate_rss/         generate_rss/
COPY generate_swagger/     generate_swagger/
COPY generate_dcat/        generate_dcat/
COPY lib/                  lib/

COPY start.sh .

ENTRYPOINT ["./start.sh"]
CMD ["./start.sh"]

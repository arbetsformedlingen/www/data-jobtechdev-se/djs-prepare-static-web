# -*- mode: cperl;-*-
package Blacklist;
use warnings;
use strict;

my $blacklistrx = "(?:".join("|",
                             'index.html',
                             'public-tokens',
                             'dcatotron',
                             'Nginx-Fancyindex-Theme-light',
                             'swagger',
                             'rss',
                             '50x.html',
                             'README',
                             'directory.dcat.json',
                             'collection.dcat.json'
                            ).")";

sub is_blacklisted {
    my $s = shift;

    if($s =~ /$blacklistrx/) {
        return 1;
    }
    return 0;
}


1;

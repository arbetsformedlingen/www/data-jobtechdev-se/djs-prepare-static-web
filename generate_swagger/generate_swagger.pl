#!/usr/bin/perl
#
# Requires a configured aws command in the environment.
#
#
use warnings;
use strict;
use File::Temp qw/ :POSIX /;
use File::Slurp;
use JSON qw( from_json );
use FindBin;
use lib $FindBin::Bin."/../lib";
use Blacklist;
use URI::Escape;


# think of SOURCE as the target
my $bucket = defined($ENV{BUCKET}) ? $ENV{BUCKET} : "data.jobtechdev.se-adhoc-test";

my $httpurl = defined($ENV{BASEURL}) ? $ENV{BASEURL} : "https://djs-frontend-data-jobtechdev-se-develop.test.services.jtech.se";



my $header = <<'END_HEADER';
{
  "x-id": [
    "data-jobtechdev-se/api"
  ],
  "openapi": "3.0.0",
  "info": {
    "version": "1.0.0",
    "title": "Open Data at Jobtech Dev",
    "description": "Here you will find the open data sets of jobtechdev.se."
  },
  "components": {
    "securitySchemes": {
      "api-key": {
        "type": "apiKey",
        "name": "api-key",
        "in": "header"
      }
    }
  },
  "paths": {
END_HEADER



sub generate_endpoint {
  my ($path, $name, $desc, $tag, $nodecount, $contenttype) = @_;

  my $endpoint = <<END_ENDPOINT;
    "$path": {
      "get": {
        "tags": [
          "$tag"
        ],
        "description": "$desc",
        "summary": "$name",
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "$contenttype": {
                "schema": {}
              },
              "application/transit+msgpack": {
                "schema": {}
              },
              "application/transit+json": {
                "schema": {}
              },
              "application/edn": {
                "schema": {}
              }
            }
          }
        }
      }
    }
END_ENDPOINT

  if ($nodecount > 0) {
    $endpoint = ",".$endpoint;
  }

  return $endpoint;
}



sub generate_tagdocs {
  my $tagdocsref = shift;
  my @tags = ();

  foreach my $tag (keys %{$tagdocsref}) {
      if(defined($tagdocsref->{use_count}->{$tag})
         && $tagdocsref->{use_count}->{$tag} > 0) {
          my $doc = $tagdocsref->{docs}->{$tag};
          $tag =~ s/[\\]//g;
          $tag =~ s/"/\\"/g;
          $doc =~ s/\\//g;
          $doc =~ s/"/\\"/g;
          $doc =~ s/\n/__JOBTECH_NEWLINE__/g;
          $doc =~ s/[[:cntrl:]]//gm;
          $doc =~ s/__JOBTECH_NEWLINE__/\\n/gm;
          $doc =~ s/^/      /gm;
          my $tagdocs = <<END_TAGDOCS;
 {
 "name": "$tag",
 "description": "$doc"
 }
END_TAGDOCS

         push(@tags, $tagdocs);
    }
  }
  return join(",\n", @tags)."\n";
}




sub proc_dir {
  my $bucket = shift;
  my $dir = shift;
  my $tagdocs = shift;
  my $endpoints = shift;

  print STDERR "entered dir $dir\n";

  my $bucketanddir = $bucket.$dir;

  my $cmd = "aws s3 ls '$bucketanddir'";
  my @list = split(/\n/, `$cmd`);

  my %file_dcat = ();

  # read all metadata
  foreach my $line (@list) {
    if ($line =~ /^([\d]{4}-[\d]{2}-[\d]{2} \d+:\d+:\d+)\s+(\d+)\s+(.+)$/) {
      my ($date, $size, $name) = ($1, $2, $3);
      if ($name =~ /^README/i) {
        my $tmpf = tmpnam();
        system("aws s3 cp 's3://$bucketanddir$name' $tmpf") == 0 || die();
        $tagdocs->{docs}->{$dir} = read_file($tmpf);
        unlink($tmpf);
        next;
      }
#      if ($name =~ /^(.+).dcat.xml$/i) {
#        next;
#      }
    }
  }

  # read all endpoints
  foreach my $line (@list) {
    if ($line =~ /^                           PRE (.*)\/$/) {
      my ($name) = ($1);
      next if(Blacklist::is_blacklisted($name));
      proc_dir($bucket, $dir."$name/", $tagdocs, $endpoints); # n.b. value by reference
    } elsif ($line =~ /^([\d]{4}-[\d]{2}-[\d]{2} \d+:\d+:\d+)\s+(\d+)\s+(.+)$/) {
        my ($date, $size, $name) = ($1, $2, $3);
        next if(Blacklist::is_blacklisted($name));
        if($name !~ /^(.+).dcat.xml$/i
           && $name !~ /^(.+).dcat.json$/i) {
          $tagdocs->{use_count}->{$dir}++;
          my $url = "$httpurl".uri_escape("$dir$name");
          print STDERR "curl: curl -k --silent --head $url\n";
          my $result = `curl -k --silent --head "$url"`;
          my $contenttype = "application/json";
          if($result !~ /content-type: (\S+)/) {
            warn "NO CONTENTTYPE: $result\n";
          } else {
            $contenttype = $1;
          }
          push(@$endpoints, generate_endpoint("$dir$name", $name, $file_dcat{$name} // $name, "$dir", scalar @$endpoints, $contenttype));
        }
    } else {
      warn("skipping line with unknown line format '$line'");
    }
  }

  if (! defined($tagdocs->{docs}->{$dir})) {
    $tagdocs->{docs}->{$dir} = "This level could use a README";
  }

  return { 'tagdocs' => $tagdocs, 'endpoints' => $endpoints };
}


my $result = proc_dir($bucket, "/", {}, []);


open(my $fh, ">swagger/swagger.json") || die();
print $fh $header;
print $fh join("\n", @{$result->{endpoints}})." },\n";
print $fh "\"tags\": [";
print $fh generate_tagdocs($result->{tagdocs});
print $fh "]\n";
print $fh "}";
close($fh);

if (system("jq . < swagger/swagger.json >/dev/null") == 0) {
  system("aws s3 cp --recursive swagger s3://$bucket/swagger") == 0 || die();
} else {
  die("Something went very wrong with the jq file");
}

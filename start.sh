#!/bin/bash

export HOME=/tmp

ln -s /secrets $HOME/.aws

cp -r generate_swagger/swagger /tmp/
cp -r generate_rss/conf /tmp/
cp generate_dcat/conf/* /tmp/conf
cp -a generate_rss/patch_scripts /tmp/

cd /tmp
#/app/generate_swagger/generate_swagger.pl
#/app/generate_rss/generate_rss.pl

#export PATH=/app/generate_dcat:$PATH
#make -f /app/generate_dcat/Makefile
